package ru.birdbook.impl.feed

import ru.birdbook.data.feed.model.FeedItemEntity
import ru.birdbook.domain.feed.model.FeedItem

fun FeedItemEntity.toFeedItem() =
    FeedItem(
        id = id.toString(),
        imageUrl = "https://birdbook.ru$oimg",
        imageThumbnailUrl = "https://birdbook.ru$img",
        nickname = pr,
        avatarUrl = pra.orEmpty(),
        species = s,
        desc = desc,
        timestamp = t.replace(".", "").substring(0, 13).toLong()
    )