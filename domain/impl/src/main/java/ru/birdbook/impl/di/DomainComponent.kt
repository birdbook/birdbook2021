package ru.birdbook.impl.di

import dagger.Component
import ru.birdbook.data.api.DataApi
import ru.birdbook.domain.api.DomainApi

@Component(
    dependencies = [DomainDependencies::class],
    modules = [DomainModule::class]
)
interface DomainComponent : DomainApi {

    companion object {

        private lateinit var component: DomainComponent

        fun provideComponent(deps: DomainDependencies): DomainApi {
            if (!Companion::component.isInitialized) {
                component = DaggerDomainComponent
                    .factory()
                    .create(deps)
            }
            return component
        }
    }

    @Component.Factory
    interface Factory {

        fun create(
            domainDeps: DomainDependencies
        ): DomainComponent
    }

    @Component(dependencies = [DataApi::class])
    interface DomainDependenciesComponent : DomainDependencies {

        @Component.Factory
        interface Factory {

            fun create(
                dataComponentApi: DataApi
            ): DomainDependencies
        }
    }
}