package ru.birdbook.impl.auth

import ru.birdbook.data.auth.AuthRepository
import ru.birdbook.domain.auth.AuthInteractor
import timber.log.Timber
import javax.inject.Inject

class AuthInteractorImpl @Inject constructor(
    private val authRepository: AuthRepository
) : AuthInteractor {

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    override suspend fun login(login: String, pass: String) =
        authRepository.login(login, pass)

}