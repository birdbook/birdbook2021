package ru.birdbook.impl.di

import ru.birdbook.data.auth.AuthRepository
import ru.birdbook.data.feed.FeedRepository

interface DomainDependencies {

    fun authRepository(): AuthRepository

    fun feedRepository(): FeedRepository

}