package ru.birdbook.impl.feed

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import ru.birdbook.data.feed.FeedRepository
import ru.birdbook.data.feed.model.FeedItemEntity
import ru.birdbook.domain.feed.FeedInteractor
import ru.birdbook.domain.feed.model.FeedItem
import javax.inject.Inject

class FeedInteractorImpl @Inject constructor(
    private val repository: FeedRepository,
) : FeedInteractor {

    private var endReached: Boolean = false

    override fun observeFeed(collection: Int): Flow<List<FeedItem>> =
        repository.observeFeed(collection)
            .map { it.map(FeedItemEntity::toFeedItem) }
            .map { it.sortedByDescending(FeedItem::timestamp) }

    override suspend fun fetch(collection: Int) {
        repository.fetchFeed(collection)
    }

    override suspend fun fetchNext(collection: Int) {
        repository.fetchNextFeedPage(collection)
    }
}