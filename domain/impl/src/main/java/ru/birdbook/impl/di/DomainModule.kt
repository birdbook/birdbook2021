package ru.birdbook.impl.di

import dagger.Binds
import dagger.Module
import ru.birdbook.domain.auth.AuthInteractor
import ru.birdbook.domain.feed.FeedInteractor
import ru.birdbook.impl.auth.AuthInteractorImpl
import ru.birdbook.impl.feed.FeedInteractorImpl

@Module
interface DomainModule {

    @Binds
    fun bindAuthRepositoryImpl(impl: AuthInteractorImpl): AuthInteractor

    @Binds
    fun bindFeedRepositoryImpl(impl: FeedInteractorImpl): FeedInteractor

}