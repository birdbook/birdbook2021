package ru.birdbook.domain.auth

interface AuthInteractor {

    suspend fun login(login: String, pass: String)
}