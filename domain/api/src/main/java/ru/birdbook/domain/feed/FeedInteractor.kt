package ru.birdbook.domain.feed

import kotlinx.coroutines.flow.Flow
import ru.birdbook.domain.feed.model.FeedItem

interface FeedInteractor {

    fun observeFeed(collection: Int): Flow<List<FeedItem>>
    suspend fun fetch(collection: Int)
    suspend fun fetchNext(collection: Int)
}