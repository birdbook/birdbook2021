package ru.birdbook.domain.api

import ru.birdbook.domain.auth.AuthInteractor
import ru.birdbook.domain.feed.FeedInteractor

interface DomainApi {

    fun getAuthInteractor(): AuthInteractor

    fun getFeedInteractor(): FeedInteractor

}