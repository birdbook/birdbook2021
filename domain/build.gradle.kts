plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

}

dependencies {
    implementation(project(Modules.common))
    api(project(Modules.domainApi))
    implementation(project(Modules.domainImpl))
    implementation(project(Modules.data))

    implementation(Libraries.Dagger.dagger)
    kapt(Libraries.Dagger.daggerCompiler)
}