package ru.birdbook.app.domain

import android.content.Context
import ru.birdbook.data.api.DataInjector
import ru.birdbook.domain.api.DomainApi
import ru.birdbook.impl.di.DaggerDomainComponent_DomainDependenciesComponent
import ru.birdbook.impl.di.DomainComponent

object DomainInjector {

    fun provideDomainApi(app: Context): DomainApi {
        val dataApi = DataInjector.provideDataApi(app)
        val domainAuthDepsComponent =
            DaggerDomainComponent_DomainDependenciesComponent.factory()
                .create(dataApi)
        return DomainComponent.provideComponent(domainAuthDepsComponent)
    }

}