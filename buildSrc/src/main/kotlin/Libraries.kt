object Libraries {

    object Kotlin {

        internal const val version = "1.5.30"

        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:$version"
        const val reflect = "org.jetbrains.kotlin:kotlin-reflect:$version"
    }

    object Coroutines {

        private const val version = "1.5.1"

        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
    }

    object AndroidX {

        private const val version = "1.3.0"
        private const val archVersion = "2.1.0"
        private const val lifecycleVersion = "2.3.1"

        const val appcompat = "androidx.appcompat:appcompat:$version"
        const val material = "com.google.android.material:material:$version"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.4"
        const val lifecycleViewModelSavedState = "androidx.lifecycle:lifecycle-viewmodel-savedstate:$lifecycleVersion"
        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

        const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:$lifecycleVersion"

        object Ktx {

            private const val fragmentVersion = "1.3.5"

            const val core = "androidx.core:core-ktx:1.5.0"
            const val lifecycleViewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"
            const val lifecycleLiveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
            const val lifecycleRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion"
            const val fragmentKtx = "androidx.fragment:fragment-ktx:$fragmentVersion"
        }

        object DataStore {

            private const val version = "1.0.0-beta02"

            const val dataStore = "androidx.datastore:datastore:$version"
            const val dataStorePreferences = "androidx.datastore:datastore-preferences:$version"
        }

    }

    object Hilt {

        const val version = "2.37"

        const val hiltAndroid = "com.google.dagger:hilt-android:$version"
        const val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:$version"
    }

    object Dagger {

        private const val version = "2.37"

        const val dagger = "com.google.dagger:dagger:$version"
        const val daggerCompiler = "com.google.dagger:dagger-compiler:$version"
    }

    object Retrofit {

        private const val version = "2.9.0"

        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val converterGson = "com.squareup.retrofit2:converter-gson:$version"

        object OkHttp {

            private const val version = "3.11.0"

            const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:$version"
        }
    }

    object Glide {

        private const val version = "4.11.0"
        const val glide = "com.github.bumptech.glide:glide:$version"
        const val glideProcessor = "com.github.bumptech.glide:compiler:$version"
    }

    object AdapterDelegates {

        private const val version = "4.3.0"

        const val kotlinDsl = "com.hannesdorfmann:adapterdelegates4-kotlin-dsl:$version"
        const val viewBinding = "com.hannesdorfmann:adapterdelegates4-kotlin-dsl-viewbinding:$version"
        const val layoutcontainer = "com.hannesdorfmann:adapterdelegates4-kotlin-dsl-layoutcontainer:$version"
        const val pagination = "com.hannesdorfmann:adapterdelegates4-pagination:$version"
    }

    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val viewBindingPropertyDelegate = "com.github.kirich1409:viewbindingpropertydelegate-noreflection:1.4.6"
    const val joda = "net.danlew:android.joda:2.10.3"

}