object Modules {

    const val common = ":common"

    const val domain = ":domain"
    const val domainApi = ":domain:api"
    const val domainImpl = ":domain:impl"

    const val data = ":data"
    const val dataApi = ":data:api"
    const val dataImpl = ":data:impl"

    object Core {

        private const val path = ":core"

        const val ui = "$path:ui"
        const val res = "$path:res"
        const val errors = "$path:errors"
        const val navigation = "$path:navigation"

        object Widgets {

            private const val path = ":core:widgets"

            const val post = "$path:post"
        }
    }

    object Features {

        private const val path = ":features"

        val feed = FeatureModule("$path:feed")
        val auth = FeatureModule("$path:auth")

        class FeatureModule(path:String){
            val api = "$path:api"
            val impl = path
        }
    }

}