object AndroidSdkConfiguration {

    const val minSdkVersion = 23
    const val compileVersion = 31
    const val targetSdkVersion = compileVersion
}