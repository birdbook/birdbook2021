plugins {
    `kotlin-dsl`
}
repositories(RepositoryHandler::mavenCentral)
dependencies {
    implementation(kotlin("stdlib-jdk8"))
}