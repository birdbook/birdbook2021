package ru.birdbook.app.common.errors

sealed class RequestError : Throwable() {
    object Network : RequestError()

    object AccessDenied : RequestError()

    object Unknown : RequestError()
}