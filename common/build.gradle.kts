plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    api(Libraries.Kotlin.stdlib)
    api(Libraries.AndroidX.Ktx.core)
    api(Libraries.Hilt.hiltAndroid)
    api(Libraries.Coroutines.core)

    api(Libraries.timber)
}