package ru.birdbook.data.core

enum class CachePolicy {
    CACHE_FIRST,
    CACHE_ONLY,
    REMOTE
}