package ru.birdbook.data.auth

interface AuthRepository {

    suspend fun login(email: String, pass: String)
//    fun register(email: String, pass: String): Completable
//    fun restore(email: String): Completable
}