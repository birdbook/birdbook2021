package ru.birdbook.data.api

import ru.birdbook.data.auth.AuthRepository
import ru.birdbook.data.feed.FeedRepository

interface DataApi {

    fun getAuthRepository(): AuthRepository

    fun getFeedRepository(): FeedRepository
}