package ru.birdbook.data.feed

import kotlinx.coroutines.flow.Flow
import ru.birdbook.data.feed.model.FeedItemEntity

interface FeedRepository {

    fun observeFeed(collection: Int): Flow<List<FeedItemEntity>>
    suspend fun fetchNextFeedPage(collection: Int)
    suspend fun fetchFeed(collection: Int)
}