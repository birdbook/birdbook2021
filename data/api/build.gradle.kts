plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

}

dependencies {
    implementation(project(Modules.common))
    implementation(Libraries.Dagger.dagger)
    implementation("com.google.code.gson:gson:2.8.8") //TODO kotlinx serialization

}