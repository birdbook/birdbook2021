package ru.birdbook.app.data.core

import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject

class ServiceFactoryImpl @Inject constructor(
    private val retrofit: Retrofit
) : ServiceFactory {

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    override fun <T> create(service: Class<T>): T = retrofit.create(service)
}