package ru.birdbook.app.data.feed

import kotlinx.coroutines.flow.Flow
import ru.birdbook.app.data.core.errors.ErrorMapper
import ru.birdbook.data.feed.FeedRepository
import ru.birdbook.data.feed.model.FeedItemEntity
import timber.log.Timber
import javax.inject.Inject

class FeedRepositoryImpl @Inject constructor(
    private val errorMapper: ErrorMapper,
    private val remoteDataSource: FeedRemoteDataSource,
    private val localDataSource: FeedLocalDataSource
) : FeedRepository {

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    override fun observeFeed(collection: Int): Flow<List<FeedItemEntity>> =
        localDataSource.observeFeed(collection)

    override suspend fun fetchNextFeedPage(collection: Int) = try {
        val feed = localDataSource.getFeed(collection)
        val lastT = localDataSource.getFeed(collection).lastOrNull()?.t
        val nextFeedPage = remoteDataSource.loadFeed(lastT, collection)
        localDataSource.saveFeed(collection, feed + nextFeedPage)
    } catch (throwable: Throwable) {
        throw errorMapper.getError(throwable)
    }

    override suspend fun fetchFeed(collection: Int) = try {
        remoteDataSource.loadFeed(null, collection).also {
            localDataSource.saveFeed(collection, it)
        }
        Unit
    } catch (throwable: Throwable) {
        throw errorMapper.getError(throwable)
    }

}
