package ru.birdbook.app.data.core.di

import dagger.Binds
import dagger.Module
import ru.birdbook.app.data.core.ServiceFactory
import ru.birdbook.app.data.core.ServiceFactoryImpl
import ru.birdbook.app.data.core.errors.ErrorMapper
import ru.birdbook.app.data.core.errors.GeneralErrorMapperImpl

@Module(includes = [OkHttpModule::class, RetrofitModule::class])
interface DataCoreModule {

    @Binds
    fun bindServiceFactory(impl: ServiceFactoryImpl): ServiceFactory

    @Binds
    fun bindErrorHandler(impl: GeneralErrorMapperImpl): ErrorMapper

}