package ru.birdbook.app.data.core.prefs

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import ru.birdbook.app.data.core.prefs.DataStorePreferenceStorage.PreferencesKeys.PREF_COOKIE
import javax.inject.Inject

interface PreferenceStorage {

    suspend fun saveCookie(cookie: String)
    suspend fun getCookie(): String?
}

class DataStorePreferenceStorage @Inject constructor(
    private val dataStore: DataStore<Preferences>
) : PreferenceStorage {

    companion object {

        const val PREFS_NAME = "birdbook"
    }

    object PreferencesKeys {

        val PREF_COOKIE = stringPreferencesKey("pref_cookie")
    }

    override suspend fun saveCookie(cookie: String) {
        dataStore.edit {
            it[PREF_COOKIE] = cookie
        }
    }

    override suspend fun getCookie(): String? =
        dataStore.data.map { it[PREF_COOKIE] }.first()

}