package ru.birdbook.app.data.api

import retrofit2.http.GET
import retrofit2.http.Query
import ru.birdbook.data.feed.model.FeedItemEntity

interface BirdbookApiService {

    @GET("/api/profile/feed")
    suspend fun feed(@Query("t") t: String?, @Query("collection") page: Int): List<FeedItemEntity>

}