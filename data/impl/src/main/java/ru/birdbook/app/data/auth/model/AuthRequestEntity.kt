package ru.birdbook.app.data.auth.model

import com.google.gson.annotations.SerializedName

data class AuthRequestEntity(
    @SerializedName("email") private val email: String,
    @SerializedName("password") private val password: String
)