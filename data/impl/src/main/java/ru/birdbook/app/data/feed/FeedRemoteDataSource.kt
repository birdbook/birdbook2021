package ru.birdbook.app.data.feed

import ru.birdbook.app.data.api.BirdbookApiService
import ru.birdbook.app.data.core.ServiceFactory
import ru.birdbook.data.feed.model.FeedItemEntity
import javax.inject.Inject

class FeedRemoteDataSource @Inject constructor(
    serviceFactory: ServiceFactory,
) {

    private val api: BirdbookApiService =
        serviceFactory.create(BirdbookApiService::class.java)

    suspend fun loadFeed(t: String?, collection: Int): List<FeedItemEntity> = api.feed(t, collection)
}