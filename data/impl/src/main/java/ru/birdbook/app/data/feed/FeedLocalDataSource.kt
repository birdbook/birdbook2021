package ru.birdbook.app.data.feed

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import ru.birdbook.data.feed.model.FeedItemEntity
import javax.inject.Inject

class FeedLocalDataSource @Inject constructor() {

    private val feedMap = mutableMapOf<Int, MutableStateFlow<List<FeedItemEntity>>>()

    fun observeFeed(collection: Int): Flow<List<FeedItemEntity>> = getStateFlowForCollection(collection)

    fun getFeed(collection: Int): List<FeedItemEntity> =
        getStateFlowForCollection(collection).value

    suspend fun saveFeed(collection: Int, list: List<FeedItemEntity>) {
        getStateFlowForCollection(collection).emit(list)
    }

    private fun getStateFlowForCollection(collection: Int) = feedMap.getOrPut(collection, { MutableStateFlow(emptyList()) })
}