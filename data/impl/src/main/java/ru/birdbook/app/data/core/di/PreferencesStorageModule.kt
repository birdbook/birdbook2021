package ru.birdbook.app.data.core.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import dagger.Module
import dagger.Provides
import ru.birdbook.app.data.core.prefs.DataStorePreferenceStorage
import ru.birdbook.app.data.core.prefs.PreferenceStorage

@Module
//@InstallIn(SingletonComponent::class)
object PreferencesStorageModule {

    val Context.dataStore: DataStore<Preferences> by preferencesDataStore(DataStorePreferenceStorage.PREFS_NAME)

    @Provides
    fun providePreferenceStorage(context: Context): PreferenceStorage =
        DataStorePreferenceStorage(context.dataStore)

}