package ru.birdbook.app.data.core

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.Interceptor
import okhttp3.Response
import ru.birdbook.app.data.core.prefs.PreferenceStorage
import java.util.HashSet

class ReceivedCookiesInterceptor(
    private val preferenceStorage: PreferenceStorage
) : Interceptor {

    @DelicateCoroutinesApi // TODO: investigate solutions
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        if (originalResponse.headers("Set-Cookie").isNotEmpty()) {
            val cookies = HashSet<String>()
            for (header in originalResponse.headers("Set-Cookie")) {
                cookies.add(header)
            }
            if (cookies.isNotEmpty()) {
                GlobalScope.launch {
                    preferenceStorage.saveCookie(cookies.elementAt(0))
                }
            }
        }
        return originalResponse
    }
}