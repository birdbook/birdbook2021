package ru.birdbook.app.data.core.errors

import ru.birdbook.app.common.errors.RequestError

interface ErrorMapper {

    fun getError(throwable: Throwable): RequestError
}