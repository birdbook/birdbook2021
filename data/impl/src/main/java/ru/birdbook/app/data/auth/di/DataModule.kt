package ru.birdbook.app.data.auth.di

import dagger.Binds
import dagger.Module
import ru.birdbook.app.data.auth.AuthRepositoryImpl
import ru.birdbook.app.data.core.di.DataCoreModule
import ru.birdbook.app.data.core.di.PreferencesStorageModule
import ru.birdbook.app.data.feed.FeedRepositoryImpl
import ru.birdbook.data.auth.AuthRepository
import ru.birdbook.data.feed.FeedRepository

@Module(includes = [DataCoreModule::class, PreferencesStorageModule::class])
interface DataModule {

    @Binds
    fun bindAuthRepositoryImpl(impl: AuthRepositoryImpl): AuthRepository

    @Binds
    fun bindFeedRepositoryImpl(impl: FeedRepositoryImpl): FeedRepository

}