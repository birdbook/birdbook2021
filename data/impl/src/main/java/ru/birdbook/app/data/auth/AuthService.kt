package ru.birdbook.app.data.auth

import retrofit2.http.Body
import retrofit2.http.POST
import ru.birdbook.app.data.auth.model.AuthRequestEntity

interface AuthService {

    @POST("/auth/login/")
    suspend fun login(@Body body: AuthRequestEntity)

//    @POST("/auth/register/")
//    fun register(@Body body: AuthRequestEntity): Completable
//
//    @POST("/auth/pr_setup/")
//    fun restore(@Body body: EmailRequestEntity): Completable

}