package ru.birdbook.app.data.auth.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ru.birdbook.data.api.DataApi

@Component(
    dependencies = [],
    modules = [DataModule::class]
)
interface DataComponent : DataApi {

    companion object {

        private lateinit var component: DataComponent

        fun provideComponent(appContext: Context): DataApi {
            if (!Companion::component.isInitialized) {
                component = DaggerDataComponent.builder()
                    .application(appContext)
                    .build()
            }
            return component
        }
    }

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Context): Builder

        fun build(): DataComponent
    }
}