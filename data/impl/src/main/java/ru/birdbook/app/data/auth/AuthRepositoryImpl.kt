package ru.birdbook.app.data.auth

import ru.birdbook.app.data.auth.model.AuthRequestEntity
import ru.birdbook.app.data.core.ServiceFactory
import ru.birdbook.app.data.core.errors.ErrorMapper
import ru.birdbook.data.auth.AuthRepository
import timber.log.Timber
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    serviceFactory: ServiceFactory,
    private val errorMapper: ErrorMapper
) : AuthRepository {

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    private val authService: AuthService = serviceFactory.create(AuthService::class.java)

    override suspend fun login(email: String, pass: String) = try {
        authService.login(AuthRequestEntity(email, pass))
        Result.success(Unit)
    } catch (throwable: Throwable) {
        Result.failure(errorMapper.getError(throwable))
    }.getOrThrow()

//    override fun register(email: String, pass: String): Completable =
//        authService.register(AuthRequestEntity(email, pass))
//            .onErrorResumeNext(errorMapper::completableError)
//
//    override fun restore(email: String): Completable =
//        authService.restore(EmailRequestEntity(email))
//            .onErrorResumeNext(errorMapper::completableError)
}
