package ru.birdbook.app.data.auth

import kotlinx.coroutines.*
import okhttp3.Interceptor
import okhttp3.Response
import ru.birdbook.app.data.core.prefs.PreferenceStorage

class AddCookiesInterceptor(
    private val preference: PreferenceStorage
) : Interceptor {

    @DelicateCoroutinesApi
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        runBlocking {
            preference.getCookie()?.let {
                builder.addHeader("Cookie", it)
            }
        }

        return chain.proceed(builder.build())
    }
}