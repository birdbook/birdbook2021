package ru.birdbook.app.data.core

interface ServiceFactory {

    fun <T> create(service: Class<T>): T

}