package ru.birdbook.app.data.core.errors

import retrofit2.HttpException
import ru.birdbook.app.common.errors.RequestError
import java.io.IOException
import java.net.HttpURLConnection.HTTP_FORBIDDEN
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED
import javax.inject.Inject

class GeneralErrorMapperImpl @Inject constructor() : ErrorMapper {

    override fun getError(throwable: Throwable): RequestError {
        return when (throwable) {
            is IOException -> RequestError.Network
            is HttpException -> {
                when (throwable.code()) {
                    HTTP_FORBIDDEN,
                    HTTP_UNAUTHORIZED -> RequestError.AccessDenied
                    else -> RequestError.Unknown
                }
            }
            else -> RequestError.Unknown
        }
    }
}