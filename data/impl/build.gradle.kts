plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")

}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        buildConfigField("String", "BASE_URL", "\"https://birdbook.ru\"")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

}

dependencies {
    implementation(project(Modules.common))
    implementation(project(Modules.dataApi))

    implementation(Libraries.AndroidX.DataStore.dataStore)
    implementation(Libraries.AndroidX.DataStore.dataStorePreferences)

    implementation(Libraries.Kotlin.stdlib)
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")

    implementation(Libraries.Retrofit.retrofit)
    implementation(Libraries.Retrofit.converterGson)
    implementation(Libraries.Retrofit.OkHttp.loggingInterceptor)

    implementation(Libraries.Dagger.dagger)
    kapt(Libraries.Dagger.daggerCompiler)
}