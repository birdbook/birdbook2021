package ru.birdbook.data.api

import android.content.Context
import ru.birdbook.app.data.auth.di.DataComponent

object DataInjector {

    fun provideDataApi(app: Context): DataApi = DataComponent.provideComponent(app)

}