plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

}

dependencies {
    implementation(project(Modules.common))
    api(project(Modules.dataApi))
    implementation(project(Modules.dataImpl))

    implementation(Libraries.Dagger.dagger)
}