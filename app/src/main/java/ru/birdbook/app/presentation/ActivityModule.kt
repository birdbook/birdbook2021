package ru.birdbook.app.presentation

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import ru.birdbook.navigation.AppRouter

@Module(includes = [ActivityModule.Declarations::class])
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Module
    @InstallIn(ActivityComponent::class)
    interface Declarations {

        @Binds
        fun bindRouter(router: AppRouterImpl): AppRouter
    }

    @Provides
    fun provideAppCompatActivity(activity: Activity): AppCompatActivity = activity as AppCompatActivity

}