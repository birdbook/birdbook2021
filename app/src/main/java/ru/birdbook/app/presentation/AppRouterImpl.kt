package ru.birdbook.app.presentation

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import ru.birdbook.navigation.AppRouter
import timber.log.Timber
import javax.inject.Inject

class AppRouterImpl @Inject constructor(
    private val activity: AppCompatActivity
) : AppRouter {

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    override fun replace(
        fragment: Fragment,
        transaction: FragmentTransaction.() -> FragmentTransaction
    ) {
        activity.supportFragmentManager
            .beginTransaction()
            .transaction()
            .replace(android.R.id.content, fragment)
            .commit()
    }
}