package ru.birdbook.app.app

import android.content.Context
import android.content.res.Resources
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.birdbook.errors.DefaultErrorConverter
import ru.birdbook.errors.ErrorConverter

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Module
    @InstallIn(SingletonComponent::class)
    interface Declarations {

        @Binds
        fun bindErrorConverter(converter: DefaultErrorConverter): ErrorConverter
    }

    @Provides
    fun provideResources(@ApplicationContext context: Context): Resources = context.resources

}