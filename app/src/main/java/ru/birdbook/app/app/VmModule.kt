package ru.birdbook.app.app

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.birdbook.app.domain.DomainInjector

@Module
@InstallIn(ViewModelComponent::class)
object VmModule {

    @Provides
    fun provideFeedInteractor(@ApplicationContext context: Context) =
        DomainInjector.provideDomainApi(context).getFeedInteractor()

}