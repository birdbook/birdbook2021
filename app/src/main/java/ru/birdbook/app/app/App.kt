package ru.birdbook.app.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import ru.birdbook.app.BuildConfig
import timber.log.Timber
import timber.log.Timber.DebugTree

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }
}