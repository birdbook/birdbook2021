plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kapt {
        javacOptions {
            // These options are normally set automatically via the Hilt Gradle plugin, but we
            // set them manually to workaround a bug in the Kotlin 1.5.20
            option("-Adagger.fastInit=ENABLED")
            option("-Adagger.hilt.android.internal.disableAndroidSuperclassValidation=true")
        }
    }

    packagingOptions {
        resources.excludes.add("META-INF/*.kotlin_module")
    }

    lint {
        isAbortOnError = false
    }
}

dependencies {

    implementation(project(Modules.common))
    implementation(project(Modules.domain))

    implementation(project(Modules.Core.ui))
    implementation(project(Modules.Core.res))
    implementation(project(Modules.Core.errors))
    implementation(project(Modules.Core.navigation))

    implementation(project(Modules.Features.auth.impl))
    implementation(project(Modules.Features.feed.impl))

    implementation(Libraries.Hilt.hiltAndroid) // ???
    kapt(Libraries.Hilt.hiltAndroidCompiler)
}