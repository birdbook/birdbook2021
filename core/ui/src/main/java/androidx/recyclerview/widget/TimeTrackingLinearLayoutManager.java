package androidx.recyclerview.widget;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.DecimalFormat;

import ru.birdbook.core.ui.BuildConfig;
import timber.log.Timber;

class TimeTrackingLinearLayoutManager extends LinearLayoutManager {

    TimeTrackingLinearLayoutManager(Context context) {
        super(context);
    }

    @NonNull
    private String tag = "*** TimeTrackingLinearLayoutManager";

    public TimeTrackingLinearLayoutManager(
            @NonNull final Context context,
            @Nullable final AttributeSet attrs,
            final int defStyleAttr,
            final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onAttachedToWindow(@NonNull final RecyclerView view) {
        super.onAttachedToWindow(view);
        tag = view.getResources().getResourceEntryName(view.getId());
    }

    @Override
    void layoutChunk(RecyclerView.Recycler recycler, RecyclerView.State state, LayoutState layoutState, LayoutChunkResult result) {
        double time = SystemClock.elapsedRealtimeNanos();
        super.layoutChunk(recycler, state, layoutState, result);
        if (BuildConfig.DEBUG) {
            time = (SystemClock.elapsedRealtimeNanos() - time) / 1000_000.0;
            Timber.d("%s layoutChunk time: %.1f", tag, time);
        }
    }
}
