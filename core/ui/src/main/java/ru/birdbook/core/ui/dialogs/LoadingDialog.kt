package ru.birdbook.core.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ru.birdbook.core.ui.R

class LoadingDialog : AppCompatDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        MaterialAlertDialogBuilder(requireActivity())
            .setView(R.layout.dialog_loading)
            .create()

    companion object {

        private val TAG = LoadingDialog::class.java.name

        fun setLoading(fragmentManager: FragmentManager?, loading: Boolean?) {
            if (fragmentManager == null || loading == null) {
                return
            }
            if (loading) {
                show(fragmentManager)
            } else {
                hide(fragmentManager)
            }
        }

        fun show(fragmentManager: FragmentManager) {
            if (fragmentManager.findFragmentByTag(TAG) == null) {
                LoadingDialog().showNow(fragmentManager, TAG)
            }
        }

        fun hide(fragmentManager: FragmentManager) {
            val dialog = fragmentManager.findFragmentByTag(TAG)
            if (dialog != null) {
                fragmentManager
                    .beginTransaction()
                    .remove(dialog)
                    .commitNowAllowingStateLoss()
            }
        }
    }
}