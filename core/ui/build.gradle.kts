plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    lint {
        isAbortOnError = false
    }
}

dependencies {

    implementation(project(Modules.common))
    api(project(Modules.Core.res))

    api(Libraries.AndroidX.appcompat)
    api(Libraries.AndroidX.material)
    api(Libraries.AndroidX.Ktx.fragmentKtx)
    api(Libraries.AndroidX.Ktx.lifecycleViewModelKtx)
}