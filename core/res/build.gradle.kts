plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    lint {
        isAbortOnError = false
    }
}

dependencies {
    implementation(project(Modules.common))
    implementation(Libraries.AndroidX.material)
}