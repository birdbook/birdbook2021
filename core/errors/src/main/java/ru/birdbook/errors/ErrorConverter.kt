package ru.birdbook.errors

interface ErrorConverter {

    fun convert(throwable: Throwable, errorId: Int = -1): ErrorItem

}