package ru.birdbook.errors

open class ErrorItem(
    val message: String,
    val code: Int = -1
)