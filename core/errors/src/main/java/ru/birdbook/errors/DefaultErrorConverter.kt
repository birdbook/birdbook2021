package ru.birdbook.errors

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.birdbook.app.common.errors.RequestError
import javax.inject.Inject

class DefaultErrorConverter @Inject constructor(
    @ApplicationContext private val context: Context
) : ErrorConverter {

    override fun convert(throwable: Throwable, errorId: Int): ErrorItem =
        when (throwable) {
            is RequestError.AccessDenied -> ErrorItem(context.resources.getString(R.string.unauthorized_error), errorId)
            is RequestError.Network -> ErrorItem(context.resources.getString(R.string.network_error), errorId)
            else -> ErrorItem(context.resources.getString(R.string.sww_error_message), errorId)
        }
}