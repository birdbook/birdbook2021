package ru.birdbook.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

interface AppRouter {

    companion object {

        val defaultTransaction: FragmentTransaction.() -> FragmentTransaction = { this }

    }

    fun replace(
        fragment: Fragment,
        transaction: FragmentTransaction.() -> FragmentTransaction = defaultTransaction
    )

}