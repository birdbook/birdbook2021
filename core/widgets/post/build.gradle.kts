plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(project(Modules.common))
    implementation(project(Modules.Core.ui))

    implementation(Libraries.Glide.glide)
    implementation(Libraries.joda)
    implementation(Libraries.AdapterDelegates.viewBinding)
}