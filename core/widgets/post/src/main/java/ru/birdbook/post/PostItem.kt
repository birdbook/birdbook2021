package ru.birdbook.post

data class PostItem(
    val id: String,
    val imageUrl: String,
    val imageThumbnailUrl: String,
    val nickname: String,
    val avatarUrl: String,
    val species: String,
    val desc: String,
    val timestamp: Long
)