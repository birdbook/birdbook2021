package ru.birdbook.post

import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import ru.birdbook.post.databinding.ItemPost2Binding

class PostViewHolder(
    private val viewBinding: ItemPost2Binding
) : RecyclerView.ViewHolder(viewBinding.root) {

    companion object {

        private var fmt: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm  d MMMM, yyyy")

        val callback: DiffUtil.ItemCallback<PostItem> =
            object : DiffUtil.ItemCallback<PostItem>() {
                override fun areItemsTheSame(oldItem: PostItem, newItem: PostItem): Boolean = oldItem.id == newItem.id

                override fun areContentsTheSame(oldItem: PostItem, newItem: PostItem): Boolean = true
            }
    }

    fun bind(item: PostItem) {
        with(viewBinding) {
            image.doOnLayout {
                image.maxHeight = image.width * 3 / 2
                image.minimumHeight = image.width
            }
        }

        with(viewBinding) {
            Glide.with(image)
                .load(item.imageUrl)
                .thumbnail(Glide.with(image).load(item.imageThumbnailUrl))
                .placeholder(R.drawable.feed_item_placeholder)
                .into(image)

            Glide.with(avatar)
                .load(item.avatarUrl)
                .placeholder(R.drawable.ic_account_placeholder)
                .circleCrop()
                .into(avatar)

            nickname.text = item.nickname
            species.text = item.species
            time.text = fmt.print(item.timestamp)
        }
    }
}

