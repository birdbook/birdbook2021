package ru.birdbook.post

import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import ru.birdbook.post.databinding.ItemPost2Binding
import timber.log.Timber

object PostDelegate {

    private var fmt: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm  d MMMM, yyyy")

    val callback: DiffUtil.ItemCallback<Any> =
        object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
                oldItem is PostItem && newItem is PostItem -> oldItem.id == newItem.id
                else -> false
            }

            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = true
        }

    fun feedItemDelegate() =
        adapterDelegateViewBinding<PostItem, Any, ItemPost2Binding>(
            { layoutInflater, root -> ItemPost2Binding.inflate(layoutInflater, root, false) }
        ) {

            with(binding) {
                image.doOnLayout {
                    Timber.d("*** ${image.width}")
                    image.maxHeight = image.width * 3 / 2
                    image.minimumHeight = image.width
                }
            }

            bind {
                with(binding) {
                    Glide.with(image)
                        .load(item.imageUrl)
                        .thumbnail(Glide.with(image).load(item.imageThumbnailUrl))
                        .placeholder(R.drawable.feed_item_placeholder)
                        .into(image)

                    Glide.with(avatar)
                        .load(item.avatarUrl)
                        .placeholder(R.drawable.ic_account_placeholder)
                        .circleCrop()
                        .into(avatar)

                    nickname.text = item.nickname
                    species.text = item.species
                    time.text = fmt.print(item.timestamp)
                }
            }
        }

}