rootProject.name = "BirdbookApp"

include(":app")
include(":common")

include(":core:ui")
include(":core:navigation")
include(":core:error")

include(":data")
include(":data:api")
include(":data:impl")

include(":domain")
include(":domain:api")
include(":domain:impl")
include(":features:auth")
include(":features:feed")
include(":core:errors")
include(":core:res")
include(":core:widgets:post")
include(":features:auth:api")
include(":features:feed:api")
