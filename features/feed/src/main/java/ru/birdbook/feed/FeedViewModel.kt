package ru.birdbook.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.birdbook.core.ui.SingleLiveEvent
import ru.birdbook.domain.feed.FeedInteractor
import ru.birdbook.domain.feed.model.FeedItem
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
internal class FeedViewModel @Inject constructor(
    private val feedUseCase: FeedInteractor
) : ViewModel() {

    private val _ptrLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val ptrLoadingLiveData: LiveData<Boolean> get() = _ptrLoadingLiveData

    private val _initLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val initLoadingLiveData: LiveData<Boolean> get() = _initLoadingLiveData

    private val _feedLiveData: MutableLiveData<List<Any>> = MutableLiveData()
    val feedLiveData: LiveData<List<Any>> get() = _feedLiveData

    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent

    private val _unauthEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
    val unauthEvent: LiveData<Any?> get() = _unauthEvent

    private var fetchFeedJob: Job? = null

    init {
        viewModelScope.launch {
            feedUseCase.observeFeed(1)
                .map { it.map(FeedItem::toPostItem) }
                .catch { Timber.e(it) }
                .onEach { Timber.d("*** update new size: ${it.size}") }
                .collect { _feedLiveData.postValue(it) }
        }
        loadFeed()
    }

    fun onRefresh() {
        fetchFeedJob = viewModelScope.launch {
            loadFeed(_ptrLoadingLiveData)
        }
    }

    fun onEndReached() {
        if (fetchFeedJob?.isActive == true) {
            return
        }
//        if (feedUseCase.isEndReached()) {
//            return
//        }
        fetchFeedJob = viewModelScope.launch {
            loadNextPage(_ptrLoadingLiveData)
        }
    }

    private fun loadFeed() {
        fetchFeedJob = viewModelScope.launch {
            loadFeed(_initLoadingLiveData)
        }
    }

    private suspend fun loadFeed(loading: MutableLiveData<Boolean>) = withContext(Dispatchers.IO) {
        try {
            loading.postValue(true)
            feedUseCase.fetch(1)
            loading.postValue(false)
        } catch (throwable: Throwable) {
            Timber.e(throwable)
            loading.postValue(false)
        }
    }

    private suspend fun loadNextPage(loading: MutableLiveData<Boolean>) = withContext(Dispatchers.IO) {
        try {
            Timber.d("*** loadNextPage")
            loading.postValue(true)
            feedUseCase.fetchNext(1)
            loading.postValue(false)
        } catch (throwable: Throwable) {
            Timber.e(throwable)
            loading.postValue(false)
        }
    }

}