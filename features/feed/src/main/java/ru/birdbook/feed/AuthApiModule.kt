package ru.birdbook.feed

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.birdbook.auth.api.FeedApi


@Module
@InstallIn(SingletonComponent::class)
object AuthApiModule {

    @Provides
    fun provideAuthApi(): FeedApi = object : FeedApi {
        override fun provideFeedFragment(): Fragment = FeedFragment()
    }
}