package ru.birdbook.feed

import ru.birdbook.domain.feed.model.FeedItem
import ru.birdbook.post.PostItem

fun FeedItem.toPostItem() = PostItem(
    id,
    imageUrl,
    imageThumbnailUrl,
    nickname,
    avatarUrl,
    species,
    desc,
    timestamp
)