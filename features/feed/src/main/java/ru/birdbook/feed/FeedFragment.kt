package ru.birdbook.feed

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.birdbook.auth.api.AuthApi
import ru.birdbook.core.ui.dialogs.LoadingDialog
import ru.birdbook.core.ui.list.EndlessRecyclerViewScrollListener
import ru.birdbook.feed.databinding.FragmentFeedBinding
import ru.birdbook.navigation.AppRouter
import ru.birdbook.post.PostAdapter
import ru.birdbook.post.PostDelegate
import ru.birdbook.post.PostItem
import ru.birdbook.post.PostViewHolder
import javax.inject.Inject

@AndroidEntryPoint
internal class FeedFragment : Fragment(R.layout.fragment_feed) {

    @Inject
    lateinit var authApi: AuthApi

    @Inject
    lateinit var appRouter: AppRouter

    private val viewModel: FeedViewModel by viewModels()
    private val viewBinding: FragmentFeedBinding by viewBinding(FragmentFeedBinding::bind)

    private val delegatesManager =
        AdapterDelegatesManager<List<Any>>()
            .addDelegate(PostDelegate.feedItemDelegate())

    private val adapter = AsyncListDifferDelegationAdapter(PostDelegate.callback, delegatesManager)
    private val adapter2 = PostAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        viewBinding.feedRecyclerView.adapter = adapter2
        viewBinding.feedRecyclerView.adapter = adapter
        viewBinding.feedRecyclerView.addOnScrollListener(RecyclerViewScrollListener(viewBinding.feedRecyclerView.layoutManager as LinearLayoutManager))
        viewBinding.swipeRefresh.setOnRefreshListener(viewModel::onRefresh)

        //adapter2.submitList()
//        viewModel.feedLiveData.observe(viewLifecycleOwner, Observer(adapter2::submitList))
        viewModel.feedLiveData.observe(viewLifecycleOwner, Observer(adapter::setItems))
        viewModel.showSwwErrorEvent.observe(viewLifecycleOwner, {
            Snackbar.make(view, getString(R.string.sww_error_message), Snackbar.LENGTH_LONG).show()
        })
        viewModel.unauthEvent.observe(viewLifecycleOwner, { logout() })
        viewModel.ptrLoadingLiveData.observe(viewLifecycleOwner, { viewBinding.swipeRefresh.isRefreshing = it })
        viewModel.initLoadingLiveData.observe(viewLifecycleOwner, { LoadingDialog.setLoading(childFragmentManager, it) })
    }

    private fun logout() {
//        logoutUseCase.logout()
//        startActivity(Intent(requireContext(), LauncherActivity::class.java))
//        requireActivity().finish()
        appRouter.replace(authApi.provideAuthFragment())
    }

    private inner class RecyclerViewScrollListener(layoutManager: LinearLayoutManager) :
        EndlessRecyclerViewScrollListener(layoutManager) {

        override fun onEndReached() = viewModel.onEndReached()
    }
}