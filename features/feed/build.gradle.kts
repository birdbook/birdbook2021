plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }



    buildFeatures {
        viewBinding = true
    }

}

dependencies {

    api(project(Modules.Features.feed.api))
    api(project(Modules.Features.auth.api))

    implementation(project(Modules.common))
    implementation(project(Modules.Core.ui))
    implementation(project(Modules.Core.navigation))
    implementation(project(Modules.domain))
    implementation(project(Modules.Core.Widgets.post))

    implementation(Libraries.AndroidX.constraintLayout)
    implementation(Libraries.AndroidX.swipeRefreshLayout)

    implementation(Libraries.joda)
    implementation(Libraries.viewBindingPropertyDelegate)
    implementation(Libraries.Glide.glide)
    implementation(Libraries.AdapterDelegates.viewBinding)


    implementation(Libraries.Hilt.hiltAndroid) //???
    kapt(Libraries.Glide.glideProcessor)
    kapt(Libraries.Hilt.hiltAndroidCompiler)
    kapt(Libraries.AndroidX.lifecycleCompiler)

}