package ru.birdbook.auth.api

import androidx.fragment.app.Fragment

interface FeedApi {

    fun provideFeedFragment(): Fragment
}