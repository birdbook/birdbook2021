plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }
}

dependencies {
    implementation(project(Modules.Core.ui))
}