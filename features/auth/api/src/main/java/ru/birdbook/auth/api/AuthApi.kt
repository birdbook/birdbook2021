package ru.birdbook.auth.api

import androidx.fragment.app.Fragment

interface AuthApi {

    fun provideAuthFragment(): Fragment
}