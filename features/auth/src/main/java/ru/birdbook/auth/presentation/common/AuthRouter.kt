package ru.birdbook.auth.presentation.common

import androidx.transition.ChangeBounds
import androidx.transition.Fade
import androidx.transition.Slide
import androidx.transition.TransitionSet
import ru.birdbook.auth.api.FeedApi
import ru.birdbook.auth.presentation.authorization.AuthorizationFragment
import ru.birdbook.auth.presentation.registration.RegistrationFragment
import ru.birdbook.auth.presentation.restore.RestoreFragment
import ru.birdbook.navigation.AppRouter
import timber.log.Timber
import javax.inject.Inject

internal class AuthRouter @Inject constructor(
    private val appRouter: AppRouter,
    private val feedApi: FeedApi,
) {

    companion object {

        private val transition = Transition()
        private val fade = Fade()
    }

    init {
        Timber.d("*** init ${this.javaClass.simpleName}")
    }

    fun routeToRegistration(authFragment: AuthorizationFragment) {
        val fragment = RegistrationFragment.newInstance()
        fragment.sharedElementEnterTransition = transition
        fragment.sharedElementReturnTransition = transition
        fragment.enterTransition = fade
        authFragment.exitTransition = fade

        appRouter.replace(fragment) {
            addSharedElement(authFragment.viewBinding.tilEmail, "til_email")
            addSharedElement(authFragment.viewBinding.tilPassword, "til_password")
            addSharedElement(authFragment.viewBinding.submitButton, "submit_button")
            addSharedElement(authFragment.viewBinding.title, "title")
            addToBackStack(null)
        }
    }

    fun routeToRestore(authFragment: AuthorizationFragment) {
        val restoreFragment = RestoreFragment()

        restoreFragment.sharedElementEnterTransition = transition
        restoreFragment.sharedElementReturnTransition = transition
        restoreFragment.enterTransition = fade
        authFragment.exitTransition = fade

        appRouter.replace(restoreFragment) {
            addSharedElement(authFragment.viewBinding.tilEmail, "til_email")
            addSharedElement(authFragment.viewBinding.submitButton, "submit_button")
            addSharedElement(authFragment.viewBinding.title, "title")
            addToBackStack(null)
        }
    }

    fun routeToFeed() {
        appRouter.replace(feedApi.provideFeedFragment())
    }

    class Transition : TransitionSet() {
        init {
            ordering = ORDERING_TOGETHER
            addTransition(ChangeBounds())
            addTransition(Slide())
        }
    }
}