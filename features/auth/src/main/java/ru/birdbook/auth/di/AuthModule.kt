package ru.birdbook.auth.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import ru.birdbook.domain.api.DomainApi
import ru.birdbook.domain.auth.AuthInteractor

@Module
@InstallIn(AuthComponent::class)
internal class AuthModule {

    @Provides
    @AuthScope
    fun providesAuthInteractor(domainApi: DomainApi): AuthInteractor = domainApi.getAuthInteractor()

}