package ru.birdbook.auth.di

import dagger.BindsInstance
import dagger.hilt.DefineComponent
import dagger.hilt.components.SingletonComponent
import ru.birdbook.domain.api.DomainApi

@AuthScope
@DefineComponent(parent = SingletonComponent::class)
internal interface AuthComponent

@DefineComponent.Builder
internal interface AuthComponentBuilder {

    fun bindDomainApi(@BindsInstance domainComponentApi: DomainApi): AuthComponentBuilder
    fun build(): AuthComponent

}