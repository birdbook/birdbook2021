package ru.birdbook.auth.presentation.authorization

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ru.birdbook.auth.BuildConfig
import ru.birdbook.auth.R
import ru.birdbook.auth.databinding.FragmentAuthBinding
import ru.birdbook.auth.presentation.common.AuthRouter
import ru.birdbook.auth.presentation.common.MessageDialogError
import ru.birdbook.core.ui.dialogs.LoadingDialog
import ru.birdbook.core.ui.dialogs.MessageDialog
import ru.birdbook.errors.ErrorItem
import javax.inject.Inject

@AndroidEntryPoint
internal class AuthorizationFragment : Fragment(R.layout.fragment_auth) {

    companion object {

        fun newInstance() = AuthorizationFragment()
    }

    @Inject
    lateinit var router: AuthRouter

    private val viewModel: AuthorizationViewModel by viewModels()
    internal val viewBinding: FragmentAuthBinding by viewBinding(FragmentAuthBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(viewBinding) {
            submitButton.setOnClickListener { viewModel.onSubmitClick(email.text.toString(), password.text.toString()) }
            signUpButton.setOnClickListener { viewModel.onSighUpClick() }
            forgetPassword.setOnClickListener { viewModel.onRestoreClick() }

            if (BuildConfig.DEBUG) {
                email.setText("nikita.bumakov@gmail.com")
                password.setText("jypzdq3UGQHRpcM")
            }
        }

        viewModel.loadingLiveData.observe(viewLifecycleOwner, { LoadingDialog.setLoading(childFragmentManager, it) })
        viewModel.showErrorEvent.observe(viewLifecycleOwner, this@AuthorizationFragment::handleError)
        viewModel.navigateEvent.observe(viewLifecycleOwner, this@AuthorizationFragment::handleNavigationEvent)
    }

    private fun handleNavigationEvent(target: AuthorizationViewModel.NavigationTarget) = when (target) {
        AuthorizationViewModel.NavigationTarget.FEED -> router.routeToFeed()
        AuthorizationViewModel.NavigationTarget.RESTORE -> router.routeToRestore(this)
        AuthorizationViewModel.NavigationTarget.REGISTRATION -> router.routeToRegistration(this)
    }

    private fun handleError(error: ErrorItem) = when (error) {
        is MessageDialogError -> MessageDialog.showWithText(childFragmentManager, error.message)
        else -> Snackbar.make(requireView(), error.message, Snackbar.LENGTH_LONG).show()
    }

}