package ru.birdbook.auth.presentation.registration

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.birdbook.auth.R

@AndroidEntryPoint
internal class RegistrationFragment : Fragment(R.layout.fragment_registration) {

    companion object {

        fun newInstance(): Fragment = RegistrationFragment()

    }

    private val viewModel: RegistrationViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel
//        submit_button.setOnClickListener {
//
//            viewModel.onSubmitClick(
//                email.text.toString(),
//                password.text.toString(),
//                password2.text.toString()
//            )
//
//            viewModel.registrationSuccessEvent.observe(
//                viewLifecycleOwner, Observer { getCallback<Callback>()?.onRegistrationSuccess() })
//            viewModel.loadingLiveData.observe(
//                viewLifecycleOwner, Observer { LoadingDialog.setLoading(childFragmentManager, it) })
//            viewModel.showDifferentPassErrorEvent.observe(
//                viewLifecycleOwner, Observer { showDifferentPassError() })
//            viewModel.showSwwErrorEvent.observe(
//                viewLifecycleOwner,
//                Observer {
//                    Snackbar.make(
//                        view,
//                        getString(RCore.string.sww_error_message),
//                        Snackbar.LENGTH_LONG
//                    ).show()
//                }
//            )
//        }
    }

//    private fun showDifferentPassError() {
//        MessageDialog.showWithText(
//            childFragmentManager,
//            getString(R.string.auth_different_passwords)
//        )
//    }
}