package ru.birdbook.auth.presentation.common

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.birdbook.app.common.errors.RequestError
import ru.birdbook.auth.R
import ru.birdbook.errors.DefaultErrorConverter
import ru.birdbook.errors.ErrorConverter
import ru.birdbook.errors.ErrorItem
import javax.inject.Inject

internal class AuthErrorConverter @Inject constructor(
    @ApplicationContext private val context: Context,
    private val defaultErrorConverter: DefaultErrorConverter
) : ErrorConverter {

    override fun convert(throwable: Throwable, errorId: Int): ErrorItem {
        if (throwable is RequestError.AccessDenied) {
            return MessageDialogError(context.getString(R.string.wrong_login_or_pass))
        }
        return defaultErrorConverter.convert(throwable, errorId)
    }
}