package ru.birdbook.auth.di

import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import ru.birdbook.domain.auth.AuthInteractor

@EntryPoint
@InstallIn(AuthComponent::class)
internal interface AuthComponentEntryPoint {

    fun getAuthInteractor(): AuthInteractor
}