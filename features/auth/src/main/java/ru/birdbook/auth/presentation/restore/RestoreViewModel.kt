package ru.birdbook.auth.presentation.restore

import androidx.lifecycle.ViewModel
import javax.inject.Inject

internal class RestoreViewModel @Inject constructor(
//    private val restoreUseCase: RestoreUseCase
) : ViewModel() {

//    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
//    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData
//
//    private val _registrationSuccessEvent: SingleLiveEvent<Void> = SingleLiveEvent()
//    val registrationSuccessEvent: LiveData<Void> get() = _registrationSuccessEvent
//
//    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
//    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent
//
//    private var registrationDisposable: Disposable? = null
//
//    override fun onCleared() {
//        registrationDisposable?.dispose()
//        super.onCleared()
//    }
//
//    fun onSubmitClick(email: String) {
//        registrationDisposable?.dispose()
//        registrationDisposable = restoreUseCase.restore(email)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .compose(RxLoadingUtils.completableLoading(_loadingLiveData))
//            .subscribe(
//                Action { _registrationSuccessEvent.postCall() },
//                RxError.error(RegistrationErrorCallback())
//            )
//    }
//
//    inner class RegistrationErrorCallback : ErrorCallbackImpl() {
//
//        override fun onNetworkError() {
//            super.onNetworkError()
//            _showSwwErrorEvent.postCall()
//        }
//
//        override fun onServerError(message: String) {
//            super.onServerError(message)
//            _showSwwErrorEvent.postCall()
//        }
//
//        override fun onUnexpectedError(throwable: Throwable) {
//            super.onUnexpectedError(throwable)
//            _showSwwErrorEvent.postCall()
//        }
//    }
}