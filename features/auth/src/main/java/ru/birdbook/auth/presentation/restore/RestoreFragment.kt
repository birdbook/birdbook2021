package ru.birdbook.auth.presentation.restore

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.birdbook.auth.R
import ru.birdbook.auth.databinding.FragmentRestoreBinding

internal class RestoreFragment : Fragment(R.layout.fragment_restore) {

    private val viewModel: RestoreViewModel by viewModels()
    private val viewBinding: FragmentRestoreBinding by viewBinding(FragmentRestoreBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        submit_button.setOnClickListener {
//            viewModel.onSubmitClick(email.text.toString())
//        }
//        viewModel.registrationSuccessEvent.observe(
//            viewLifecycleOwner, Observer { getCallback<Callback>()?.onRestoreSuccess() })
//        viewModel.loadingLiveData.observe(
//            viewLifecycleOwner, Observer { LoadingDialog.setLoading(childFragmentManager, it) })
//        viewModel.showSwwErrorEvent.observe(
//            viewLifecycleOwner,
//            Observer {
//                Snackbar.make(
//                    view,
//                    getString(RCore.string.sww_error_message),
//                    Snackbar.LENGTH_LONG
//                ).show()
//            }
//        )
    }
}