package ru.birdbook.auth.presentation.common

import ru.birdbook.errors.ErrorItem

internal class MessageDialogError(
    message: String
) : ErrorItem(message)