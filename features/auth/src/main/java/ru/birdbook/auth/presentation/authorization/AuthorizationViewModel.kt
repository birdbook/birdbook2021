package ru.birdbook.auth.presentation.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.EntryPoints
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.birdbook.auth.di.AuthComponentEntryPoint
import ru.birdbook.auth.di.AuthComponentManager
import ru.birdbook.auth.presentation.common.AuthErrorConverter
import ru.birdbook.core.ui.SingleLiveEvent
import ru.birdbook.domain.auth.AuthInteractor
import ru.birdbook.errors.ErrorItem
import javax.inject.Inject

@HiltViewModel
internal class AuthorizationViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val errorConverter: AuthErrorConverter,
    authComponentManager: AuthComponentManager
) : ViewModel() {

    private val authInteractor: AuthInteractor by lazy(
        EntryPoints.get(
            authComponentManager.authComponent,
            AuthComponentEntryPoint::class.java
        )::getAuthInteractor
    )

    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData

    private val _showErrorEvent: SingleLiveEvent<ErrorItem> = SingleLiveEvent()
    val showErrorEvent: LiveData<ErrorItem> get() = _showErrorEvent

    private val _navigateEvent: SingleLiveEvent<NavigationTarget> = SingleLiveEvent()
    val navigateEvent: LiveData<NavigationTarget> get() = _navigateEvent

    fun onSubmitClick(email: String, pass: String) {
        viewModelScope.launch {
            login(email, pass)
        }
    }

    fun onSighUpClick() {
        _navigateEvent.postValue(NavigationTarget.REGISTRATION)
    }

    fun onRestoreClick() {
        _navigateEvent.postValue(NavigationTarget.RESTORE)
    }

    private suspend fun login(email: String, password: String) = withContext(Dispatchers.IO) {
        try {
            _loadingLiveData.postValue(true)
            authInteractor.login(email, password)
            handleSuccessfulLogin()
        } catch (throwable: Throwable) {
            _loadingLiveData.postValue(false)
            _showErrorEvent.postValue(errorConverter.convert(throwable))
        }
    }

    private fun handleSuccessfulLogin() {
        _loadingLiveData.postValue(false)
        _navigateEvent.postValue(NavigationTarget.FEED)
    }

    enum class NavigationTarget {
        FEED,
        RESTORE,
        REGISTRATION,
    }
}