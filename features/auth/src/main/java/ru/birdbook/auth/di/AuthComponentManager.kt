package ru.birdbook.auth.di

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.birdbook.app.domain.DomainInjector
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class AuthComponentManager @Inject constructor(
    authComponentBuilder: AuthComponentBuilder,
    @ApplicationContext private val context: Context
) {

    val authComponent: AuthComponent

    init {
        val domain = DomainInjector.provideDomainApi(app = context)
        authComponent = authComponentBuilder.bindDomainApi(domain).build()
    }

}