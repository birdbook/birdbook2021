package ru.birdbook.auth.presentation.registration

import androidx.lifecycle.ViewModel
import dagger.hilt.EntryPoints
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.birdbook.auth.di.AuthComponentEntryPoint
import ru.birdbook.auth.di.AuthComponentManager
import ru.birdbook.domain.auth.AuthInteractor
import javax.inject.Inject

@HiltViewModel
internal class RegistrationViewModel @Inject constructor(
    authComponentManager: AuthComponentManager
) : ViewModel() {

    private val authInteractor: AuthInteractor by lazy(
        EntryPoints.get(
            authComponentManager.authComponent,
            AuthComponentEntryPoint::class.java
        )::getAuthInteractor
    )

//    private val _loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
//    val loadingLiveData: LiveData<Boolean> get() = _loadingLiveData
//
//    private val _registrationSuccessEvent: SingleLiveEvent<Void> = SingleLiveEvent()
//    val registrationSuccessEvent: LiveData<Void> get() = _registrationSuccessEvent
//
//    private val _showDifferentPassErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
//    val showDifferentPassErrorEvent: LiveData<Any?> get() = _showDifferentPassErrorEvent
//
//    private val _showSwwErrorEvent: SingleLiveEvent<Any?> = SingleLiveEvent()
//    val showSwwErrorEvent: LiveData<Any?> get() = _showSwwErrorEvent

//    override fun onCleared() {
//        registrationDisposable?.dispose()
//        super.onCleared()
//    }

    init {
    }

//    fun onSubmitClick(email: String, pass: String, pass2: String) {
//        registrationDisposable?.dispose()
//        registrationDisposable = registerUseCase.register(email, pass, pass2)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .compose(RxLoadingUtils.completableLoading(_loadingLiveData))
//            .subscribe(
//                Action { _registrationSuccessEvent.postCall() },
//                RxError.error(RegistrationErrorCallback())
//            )
//    }

//    inner class RegistrationErrorCallback : ErrorCallbackImpl() {
//
//        override fun onNetworkError() {
//            super.onNetworkError()
//            _showSwwErrorEvent.postCall()
//        }
//
//        override fun onServerError(message: String) {
//            super.onServerError(message)
//            _showSwwErrorEvent.postCall()
//        }
//
//        override fun onUnexpectedError(throwable: Throwable) {
//            super.onUnexpectedError(throwable)
//            if (throwable is DifferentPasswordsException) {
//                _showDifferentPassErrorEvent.postCall()
//            } else {
//                _showSwwErrorEvent.postCall()
//            }
//        }
//    }
}