package ru.birdbook.auth.di

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.birdbook.auth.api.AuthApi
import ru.birdbook.auth.presentation.authorization.AuthorizationFragment


@Module
@InstallIn(SingletonComponent::class)
object AuthApiModule {

    @Provides
    fun provideAuthApi(): AuthApi = object : AuthApi {
        override fun provideAuthFragment(): Fragment = AuthorizationFragment.newInstance()

    }
}