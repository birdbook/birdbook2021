plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = AndroidSdkConfiguration.compileVersion

    defaultConfig {
        minSdk = AndroidSdkConfiguration.minSdkVersion
        targetSdk = AndroidSdkConfiguration.targetSdkVersion
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        viewBinding = true
    }

}

dependencies {

    api(project(Modules.Features.auth.api))
    api(project(Modules.Features.feed.api))

    implementation(project(Modules.common))
    implementation(project(Modules.Core.ui))
    implementation(project(Modules.Core.errors))
    implementation(project(Modules.Core.navigation))
    implementation(project(Modules.domain))

    implementation(Libraries.AndroidX.constraintLayout)
    implementation(Libraries.viewBindingPropertyDelegate)
    kapt(Libraries.AndroidX.lifecycleCompiler)

    implementation(Libraries.Hilt.hiltAndroid) //???
    kapt(Libraries.Hilt.hiltAndroidCompiler)

}